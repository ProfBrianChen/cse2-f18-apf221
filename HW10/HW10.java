////Andrew Fedun 12/2/18 HW 10 
////CSE 2 Arielle Carr
///This program is designed to simulate a tic tac toe game, showing win lose or draw


import java.util.Scanner;
public class HW10 {
	
	///prints the tic tac toe board
	public static void print(String[][] tb) {
		for (int i=0; i<tb.length;i++) {//run for every row
			for (int j=0; j<tb.length;j++) {//run for every column within the row
				System.out.print(tb[i][j]+" ");//print corresponding array
			}
			System.out.println();//print new line
		}
	}
	
	////checks if input is valid
	public static int check(int player, String token, int[]chain) {
		int x=100;//creates variable to hold value to be returned
		boolean check= false;//creates boolean to check for integer input
		Scanner scnr = new Scanner(System.in);//creates scanner
		int temp=99;//creates variable to temporarily hold values
		System.out.println("Player "+player+ ", please enter your placement (1-9) of the "+token+" token:");//prompts player to enter value
				
		
		
		while(x==100) {//run until valid input is saved to x
			check= scnr.hasNextInt();//check if user entered integer
			if(check) {//if it was an integer
				temp=scnr.nextInt();//save to temp
				while(temp<1||temp>9) {//if temp is out of range
					System.out.println("Not within the range of 1-9, enter new: ");
					temp=scnr.nextInt();//save new input to temp
				}
				
				for(int i=0; i<chain.length;i++) {//run for every element entered
					if(temp==chain[i]) {//if temp=a previous value, prompt for new
						System.out.println("That spot is taken, please enter new: ");
						temp=scnr.nextInt();
					}
				}
				
				x=temp;//once it makes it through all tests, set temp to x
				
			}
			
			else {
				scnr.next();//clear scanner
				System.out.println("Not an integer, please try again - enter new integer 1-9:");//prompt for new input
			}			
		}
		return x;//return x to main
	}
	

	///create new board
	public static String[][] newboard(String[][] tb, int move, int player){
		String rep = "error";//create value for users token
		if (player==1) {
			rep = "O";//for player 1, token is O
		}
		else if(player==2) {
			rep = "X";//for player 2, token is X
		}
		
		switch(move) {
		case 1://if user enters 1, change the value at row 0, column 0
			tb[0][0]=rep;
			break;
		case 2:
			tb[0][1]=rep;
			break;
		case 3:
			tb[0][2]=rep;
			break;	
		case 4:
			tb[1][0]=rep;
			break;
		case 5:
			tb[1][1]=rep;
			break;
		case 6:
			tb[1][2]=rep;
			break;
		case 7:
			tb[2][0]=rep;
			break;
		case 8:
			tb[2][1]=rep;
			break;
		case 9:
			tb[2][2]=rep;
			break;	
		}
		return tb;//return new board
	}
	
	///checks for a win
	public static int wincheck(String[][]tb,int track) {
		int wincheck=0;//create variable to hold win info
		
		///horizontal
		if(tb[0][0]==tb[0][1]&&tb[0][1]==tb[0][2]&&tb[0][2]=="O") {//if all vals in row 0 are O, player 1 won
			wincheck=1;//1 means player 1 won
		}
		else if(tb[0][0]==tb[0][1]&&tb[0][1]==tb[0][2]&&tb[0][2]=="X") {//if all vals in row 0 are X, player 2 won
			wincheck=2;//2 means player 2 won
		}
		else if(tb[1][0]==tb[1][1]&&tb[1][1]==tb[1][2]&&tb[1][2]=="O") {
			wincheck=1;
		}
		else if(tb[1][0]==tb[1][1]&&tb[1][1]==tb[1][2]&&tb[1][2]=="X") {
			wincheck=2;
		}
		else if(tb[2][0]==tb[2][1]&&tb[2][1]==tb[2][2]&&tb[2][2]=="O") {
			wincheck=1;
		}
		else if(tb[2][0]==tb[2][1]&&tb[2][1]==tb[2][2]&&tb[2][2]=="X") {
			wincheck=2;
		}
		
		///verticals
		else if(tb[0][0]==tb[1][0]&&tb[1][0]==tb[2][0]&&tb[2][0]=="O") {//if all vals in column 0 are O, player 1 won
			wincheck=1;
		}
		else if(tb[0][0]==tb[1][0]&&tb[1][0]==tb[2][0]&&tb[2][0]=="X") {//if all vals in column 0 are X, player 2 won
			wincheck=2;
		}
		else if(tb[0][1]==tb[1][1]&&tb[1][1]==tb[2][1]&&tb[2][1]=="O") {
			wincheck=1;
		}
		else if(tb[0][1]==tb[1][1]&&tb[1][1]==tb[2][1]&&tb[2][1]=="X") {
			wincheck=2;
		}
		else if(tb[0][2]==tb[1][2]&&tb[1][2]==tb[2][2]&&tb[2][2]=="O") {
			wincheck=1;
		}
		else if(tb[0][2]==tb[1][2]&&tb[1][2]==tb[2][2]&&tb[2][2]=="X") {
			wincheck=2;
		}
		
		
		////diagonals
		else if(tb[0][0]==tb[1][1]&&tb[1][1]==tb[2][2]&&tb[2][2]=="O") {//if all vals in diagonal O, player 1 won
			wincheck=1;
		}
		else if(tb[0][0]==tb[1][1]&&tb[1][1]==tb[2][2]&&tb[2][2]=="X") {//if all vals in diagonal X, player 2 won
			wincheck=2;
		}
		else if(tb[0][2]==tb[1][1]&&tb[1][1]==tb[2][0]&&tb[2][0]=="O") {
			wincheck=1;
		}
		else if(tb[0][2]==tb[1][1]&&tb[1][1]==tb[2][0]&&tb[2][0]=="X") {
			wincheck=2;
		}
		
		///draw
		else if(track==9) {
			wincheck=3;//if all turns are used and none of the above statements are true, then its a draw
		}
		
		
		
		
		return wincheck;//return val of win type
	}
	
	
	
	
	public static void main(String[]args) {
		String[][] tb = {//create og tic tac toe board
				{"1","2","3"},
				{"4","5","6"},
				{"7","8","9"}
		};
		Scanner scnr = new Scanner(System.in);//create scanner
		int move = 99;//create var for move
		int player=1;//start at player 1
		print(tb);//call print method to print original board
		int track=1;//create tracker
		String token="error";//create var for the token
		int[] chain=new int[15];//create array to remember all values entered
		int wincheck=0;//create variable for checking win status
		
		
		while(wincheck==0) {//run until some type of win occurs
			if(track%2==1) {//if tracker is odd, its player 1s turn
				player=1;
				token="O";
			}
			else if(track%2==0) {//if tracker is even, its player 2s turn
				player =2;
				token="X";
			}
			
			
		
			move=check(player,token,chain);//call move method
			chain[track]=move;//save move to array tracking past moves
			
		
			tb=newboard(tb,move,player);//call newboard method to update for players move
			System.out.println("new board");
			print(tb);//print newboard
			
			wincheck=wincheck(tb,track);//call wincheck
			
			if(wincheck==1) {//if returned 1, p1 won
				System.out.println("Congratulations Player 1, you won!");
			}
			else if(wincheck==2) {
				System.out.println("Congratulations Player 2, you won!");
			}
			else if(wincheck==3) {
				System.out.println("Draw!");
			}

			
			track++;//increment tracker
		
		
		}
		
	}

}