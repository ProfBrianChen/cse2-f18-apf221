//////Andrew Fedun HW 7 11/14/18
//////Arielle Carr
////This program is designed to print a deck of cards, shuffle it, and pick hands off the top of the deck 



import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class hw08 {
	public static void printArray(String[] cards) {		
		for (int i =0; i<cards.length; i++) { //run through entire length of array
		System.out.print(cards[i]+" "); //print the item with a space
		}
	}
//////////////////	
	public static String[] shuffle(String[] cards) {
		Random rand = new Random();	 //create random number generator
		for(int j = 0; j<80;j++) { //make replacements 80 times
			int k =rand.nextInt(51)+1; //pick a card 
			String temp = "error"; //create holder for card 0
			temp = cards[0]; //hold card 0
			cards[0] = cards[k]; //set card 0 to card k
			cards[k] = temp; //set card k to original val of card 0		
		}				
		return cards; //return the shuffled set of cards
	}
//////////////
	public static String[] getHand(String[] cards, int index, int numCards) {
		String[]hand = new String[5]; //create array for the hand to return
		System.out.println();
		System.out.println("Hand:");
		
		if(index-numCards<0) { //if not enough cards, let user know they have to restart
			System.out.println("You're out of cards! Please enter 0 and start over with a new deck");

		}
		
		else {
		for (int y=index-numCards; y<=index;y++) { //pick up the last numCards off the deck
				int p = 0; //create counter to keep track of hand
			    hand[p] = cards[y]; //set hand to save top card values
			    System.out.print(hand[p]+" "); //print hand cards
			    p++; //increment counter
	}

		System.out.println();
		}
		return hand;
	}
	
	public static void main(String[]args) {
		Scanner scan = new Scanner(System.in); //create scanner
				
			
		//suits club, heart, spade or diamond
		String[] suitNames = {"C","H","S","D"};
		String[] rankNames = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
		String[] cards = new String[52];
		String[] hand = new String[5];
		int numCards = 5;
		int again = 1;
		int index = 51;
		
			
		for (int i=0; i<52; i++) {//for cards up to 52
			
			cards[i] =  rankNames[i%13]+suitNames[(i/13)]; //creates card as the combination of rank and suit names
			System.out.print(cards[i]+" "); //print cards
		}
		
		System.out.println();
		String shuffled[] = shuffle(cards);//store shuffled deck
		
		System.out.println("Shuffled:");
		printArray(shuffled);//print shuffled
		
		while(again==1) {//run until user says they dont want to
			getHand(shuffled, index, numCards);//call method getHand to get the hand
			
			index = index - numCards; //reduce index by number of cards drawn
			System.out.println("Enter a 1 if you want another hand drawn"); //prompt user for their preference
			again = scan.nextInt(); //store user preference
		}
		
		
	}

}
