import java.util.Scanner;

public class Average{
  public static void main (String [] args){
    Scanner scan = new Scanner(System.in);
    
    System.out.println("input 3 integers");
    int num1 = scan.nextInt();
    int num2 = scan.nextInt();
    int num3 = scan.nextInt();
    
    double avg = (num1 + num2 + num3) / 3.0;
    
    if ( avg>0 )
      System.out.println("The average is positive");
    if (avg == 0)
      System.out.println("The average is zero");
    if (avg < 0)
      System.out.println("The average is negative");
  }
}