import java.util.Scanner;

public class ifelse{
  public static void main(String[] args){
    boolean notify;
    
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter true to sign up and false to opt out: ");
    notify = myScanner.nextBoolean();
    
    if (notify){
      System.out.println("Youve signed up!");
    }
      
    
    
    if (!notify){
      System.out.println("You may always opt in at a later time.");
    }
  
  }
}