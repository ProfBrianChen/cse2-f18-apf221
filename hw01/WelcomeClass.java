/////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///displays a welcome to the class from me
    
    
    System.out.println("  -----------"); //prints first line
    System.out.println("  | WELCOME |"); //prints second line
    System.out.println("  -----------"); //prints third line
    System.out.println("  ^  ^  ^  ^  ^  ^"); // prints fourth line
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); //prints fifth line
    System.out.println("<-A--P--F--2--2--1->"); //prints sixth line
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); //prints seventh line
    System.out.println("  v  v  v  v  v  v"); //prints eighth line
    
    System.out.println("My name is Andrew Fedun and I love music, mashed potatoes, and adventures."); //prints first line of bio
    System.out.println("I am majoring in Industrial Systems Engineering and Finance."); //prints second line of bio
    
      
  
    
  }
}