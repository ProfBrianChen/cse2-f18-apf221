/////////
////// Andrew Fedun CSE 02 Section 110
//// CSE 02 HW 2 Arithmetic
// September 10 2018

public class Arithmetic{
  
  public static void main(String args[]){
  //Calculates and displays various costs and taxes from a shopping trip

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    
    
    double totalCostOfPants; //total cost of pants
    double totalCostOfShirts; //total cost of shirts
    double totalCostOfBelts;  //total cost of belts
    double totalTaxOnPants; //total tax on pants
    double totalTaxOnShirts; //total tax on shirts
    double totalTaxOnBelts; //total tax on belts
    double totalCostBeforeTaxes; //total cost of purchases before taxes
    double totalSalesTax; //total sales taxes
    double totalPaid; //total paid for entire transaction
    
    totalCostOfPants = pantsPrice * numPants; //calculates total cost of pants
    totalCostOfShirts = shirtPrice * numShirts; //calculates total cost of sweatshirts
    totalCostOfBelts = beltCost * numBelts; //calculates total cost of belts
    totalCostBeforeTaxes = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //calculates total cost before taxes
    
    totalTaxOnPants = (totalCostOfPants * paSalesTax) * 100; //inital and intermediate calculation for tax on pants 
      totalTaxOnPants = (int) totalTaxOnPants; //intermediate step to ensure only 2 decimal places
      totalTaxOnPants = totalTaxOnPants / 100.0; //final step for calculating tax on pants with 2 decimal places
    totalTaxOnShirts = (totalCostOfShirts * paSalesTax) * 100; //inital and intermediate calculation for tax on sweatshirts
      totalTaxOnShirts = (int) totalTaxOnShirts; //intermediate step to ensure only 2 decimal places
      totalTaxOnShirts = totalTaxOnShirts / 100.0; //final step for calculating tax on sweatshirts with 2 decimal places
    totalTaxOnBelts = totalCostOfBelts * paSalesTax * 100; //inital and intermediate calculation for tax on belts
      totalTaxOnBelts = (int) totalTaxOnBelts; //intermediate step to ensure only 2 decimal places
      totalTaxOnBelts = totalTaxOnBelts / 100.0; //final step for calculating tax on belts with 2 decimal places
    
    
    totalSalesTax = (totalTaxOnPants + totalTaxOnShirts + totalTaxOnBelts) * 100; ////inital and intermediate calculation for tax on all items
      totalSalesTax = (int) totalSalesTax; //intermediate step to ensure only 2 decimal places
      totalSalesTax = totalSalesTax / 100.0; //final step for calculating tax on all items with 2 decimal places
    totalPaid = totalCostBeforeTaxes + totalSalesTax; //calclulates total amount paid for items and taxes
    
    
    System.out.println("The total cost of pants is: " + totalCostOfPants); //prints the total cost of pants
    System.out.println("The total tax paid on pants is: " + totalTaxOnPants); //prints the total tax paid on pants
    System.out.println("The total cost of sweatshirts is: " + totalCostOfShirts); //prints the total cost of sweatshirts
    System.out.println("The total tax paid on sweatshirts is: " + totalTaxOnShirts); //prints the total tax paid on sweatshirts
    System.out.println("The total cost of belts is: " + totalCostOfBelts); //prints the total cost of belts
    System.out.println("The total tax paid on belts is: " + totalTaxOnBelts); //prints the total tax paid on belts
    System.out.println("The total cost of purchases pre-tax is: " + totalCostBeforeTaxes); //prints the total cost of items before tax
    System.out.println("The total sales tax is: " + totalSalesTax); //prints the total taxes paid 
    System.out.println("The total cost of purchases including sales tax is: " + totalPaid); //prints the total cost of purchases including sales tax
    
    
  }
  
}
    
    
    