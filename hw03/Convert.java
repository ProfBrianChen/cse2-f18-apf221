//////// Andrew Fedun CSE 02 Section 110
/////// HW 3 Convert 
///// September 17 2018

/* This program is designed to take input of acres of rainfall and inches of rainfall to compute
the quantity of rain in cubic miles */

import java.util.Scanner; //imports the utility to enable Scanner for user input

public class Convert{
  // main method required for every Java program - this program is called Convert
  public static void main (String[] args){
    
Scanner myScanner = new Scanner(System.in); //declares scanner so it can be used
    System.out.print("Enter the affected area in acres: "); //prompts the user to enter affected area in acres
    double areaAcres = myScanner.nextDouble(); //saves the users input as a double areaAcres
    
    System.out.print("Enter the rainfall in the affected area in inches: "); //prompts the user to enter rainfall in inches
    double rainInches = myScanner.nextDouble(); //saves the users input as a double rainInches
    
    double acreSqMi = 0.0015625; //declares and provides the conversion factor for acres to square miles for later calculation
    double inchMi = 1.0 / (12.0 * 5280.0); //declares and provides the conversion factor for inches to miles for later calculation
    
    double areaSqMi = areaAcres * acreSqMi; //converts the area from acres to square miles using conversion factor
    double heightMi = rainInches * inchMi; //converts the height from inches to miles using conversion factor 
    double cubicMi = areaSqMi * heightMi; //calculates total cubic miles by multiplying area by height
    
    System.out.print("The quantity of rain in cubic miles is: " + cubicMi); //displays to the user the quantity of rain in cubic miles 
    
    
    
  }
  
}