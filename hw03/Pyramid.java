//////// Andrew Fedun CSE 02 Section 110
/////// HW 3 Pyramid
///// September 17 2018

/* This program is designed to take user input for height and square side of a pyramid
and output its calculation for the volume */

import java.util.Scanner; //imports the Scanner utility to enable user input

public class Pyramid{
  // main method required for every Java program - this program is called Pyramid
  public static void main (String [] args){
  
  Scanner myScanner = new Scanner(System.in); //declares a Scanner to allow for user input
  
  System.out.print("Please enter the square side of the pyramid: "); //prompts the user to enter the square side of the pyramid
  double squareSide = myScanner.nextDouble(); //saves the user input in the variable squareSide
  
  System.out.print("Please enter the height of the pyramid: "); //prompts the user to enter the height of the pyramid
  double height = myScanner.nextDouble(); //saves the user input in the variable height
  
  double volume = (squareSide * squareSide * height) / 3; //calcates the volume of the pyramid as (l*w*h)/3
  
  System.out.print("The volume inside the pyramid is: " + volume); //displays to the user the calculated volume of the pyramid
  
    }
  }