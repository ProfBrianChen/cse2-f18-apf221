//////// Andrew Fedun CSE 02 Section 110
/////// HW 4 CrapsIf
///// September 24 2018

/* This program is designed to either generate random dice values or take user inputs. 
it then generates the corresponding craps slang using if statements. if improper dice values are entered, it asks the user to try again*/

// I had to do the assignment twice because codeanywhere deleted my first version. Please keep this in mind when grading :)


import java.util.Scanner; //imports scanner
import java.util.Random; //imports rand num generator

public class CrapsIf{
  public static void main (String[]args){
    Scanner scnr = new Scanner(System.in); //declares scnr
    Random rand = new Random(); //declares rand
    
    System.out.println("Please enter either 1 for randomly generated dice or 2 to input your dice"); //prompts user to enter preference
    int pref = scnr.nextInt();//saves preference
    int d1, d2;//declares dice 1 & 2
    
    
    //if user preference for random generated dice, run this branch
    if (pref == 1){ 
      d1 = rand.nextInt(6) + 1; //randomly generates dice value 1-6
      d2 = rand.nextInt(6) + 1;
      
      ////column 1
      if ((d1==1) && (d2==1)){ //if both dice are 1 print this message
        System.out.println("Snake Eyes");
      }
      
      //at this point the code becomes largely repetitive.from here i will comment just on non repetitive code
      
      else if (((d1==1) && (d2==2)) || ((d1==2) && (d2==1))){
        System.out.println("Ace Deuce");
      }
      else if (((d1==1) && (d2==3)) || ((d1==3) && (d2==1))){
        System.out.println("Easy Four");
      }
      else if (((d1==1) && (d2==4)) || ((d1==4) && (d2==1))){
        System.out.println("Fever Five");
      }
      else if (((d1==1) && (d2==5)) || ((d1==5) && (d2==1))){
        System.out.println("Easy Six");
      }
      else if (((d1==1) && (d2==6)) || ((d1==6) && (d2==1))){
        System.out.println("Seven Out");
      }
      
      ////coulumn 2
      if ((d1==2) && (d2==2)){
        System.out.println("Hard Four");
      }
      else if (((d1==2) && (d2==3)) || ((d1==3) && (d2==2))){
        System.out.println("Fever Five");
      }
      else if (((d1==2) && (d2==4)) || ((d1==4) && (d2==2))){
        System.out.println("Easy Six");
      }
      else if (((d1==2) && (d2==5)) || ((d1==5) && (d2==2))){
        System.out.println("Seven Out");
      }
      else if (((d1==2) && (d2==6)) || ((d1==6) && (d2==2))){
        System.out.println("Easy Eight");
      }
      
      ////coulumn 3
      if ((d1==3) && (d2==3)){
        System.out.println("Hard Six");
      }
      else if (((d1==3) && (d2==4)) || ((d1==4) && (d2==3))){
        System.out.println("Seven Out");
      }
      else if (((d1==3) && (d2==5)) || ((d1==5) && (d2==3))){
        System.out.println("Easy Eight");
      }
      else if (((d1==3) && (d2==6)) || ((d1==6) && (d2==3))){
        System.out.println("Nine");
      }
      
      ////column 4
      if ((d1==4) && (d2==4)){
        System.out.println("Hard Eight");
      }
      else if (((d1==4) && (d2==5)) || ((d1==5) && (d2==4))){
        System.out.println("Nine");
      }
      else if (((d1==4) && (d2==6)) || ((d1==6) && (d2==4))){
        System.out.println("Easy Ten");
      }
      
      ////column 5
      if ((d1==5) && (d2==5)){
        System.out.println("Hard Ten");
      }
      else if (((d1==5) && (d2==6)) || ((d1==6) && (d2==5))){
        System.out.println("Yo-leven");
      }
      
      ////column 6
      if ((d1==6) && (d2==6)){
        System.out.println("Boxcars");
      }
      
    }
    
    /////// 
    
    
    else if (pref == 2){
      System.out.println("Please enter the number on your first dice"); //promptsuser for val of first dice
      d1 = scnr.nextInt();//saves first dice
      System.out.println("Please enter the number on your second dice");
      d2 = scnr.nextInt();
      
      
      ////column 1
      if ((d1==1) && (d2==1)){
        System.out.println("Snake Eyes");
      }
      else if (((d1==1) && (d2==2)) || ((d1==2) && (d2==1))){
        System.out.println("Ace Deuce");
      }
      else if (((d1==1) && (d2==3)) || ((d1==3) && (d2==1))){
        System.out.println("Easy Four");
      }
      else if (((d1==1) && (d2==4)) || ((d1==4) && (d2==1))){
        System.out.println("Fever Five");
      }
      else if (((d1==1) && (d2==5)) || ((d1==5) && (d2==1))){
        System.out.println("Easy Six");
      }
      else if (((d1==1) && (d2==6)) || ((d1==6) && (d2==1))){
        System.out.println("Seven Out");
      }
      
      ////coulumn 2
      else if ((d1==2) && (d2==2)){
        System.out.println("Hard Four");
      }
      else if (((d1==2) && (d2==3)) || ((d1==3) && (d2==2))){
        System.out.println("Fever Five");
      }
      else if (((d1==2) && (d2==4)) || ((d1==4) && (d2==2))){
        System.out.println("Easy Six");
      }
      else if (((d1==2) && (d2==5)) || ((d1==5) && (d2==2))){
        System.out.println("Seven Out");
      }
      else if (((d1==2) && (d2==6)) || ((d1==6) && (d2==2))){
        System.out.println("Easy Eight");
      }
      
      ////coulumn 3
      else if ((d1==3) && (d2==3)){
        System.out.println("Hard Six");
      }
      else if (((d1==3) && (d2==4)) || ((d1==4) && (d2==3))){
        System.out.println("Seven Out");
      }
      else if (((d1==3) && (d2==5)) || ((d1==5) && (d2==3))){
        System.out.println("Easy Eight");
      }
      else if (((d1==3) && (d2==6)) || ((d1==6) && (d2==3))){
        System.out.println("Nine");
      }
      
      ////column 4
      else if ((d1==4) && (d2==4)){
        System.out.println("Hard Eight");
      }
      else if (((d1==4) && (d2==5)) || ((d1==5) && (d2==4))){
        System.out.println("Nine");
      }
      else if (((d1==4) && (d2==6)) || ((d1==6) && (d2==4))){
        System.out.println("Easy Ten");
      }
      
      ////column 5
      else if ((d1==5) && (d2==5)){
        System.out.println("Hard Ten");
      }
      else if (((d1==5) && (d2==6)) || ((d1==6) && (d2==5))){
        System.out.println("Yo-leven");
      }
      
      ////column 6
      else if ((d1==6) && (d2==6)){
        System.out.println("Boxcars");
      }
      
      
      /////if they messed up their dice
      else if ((d1>6) || (d1<1)){ //if user vals fall outside 1-6, correct them
        System.out.println("Please try again and enter values between 1-6");
      }
      else if ((d2>6) || (d2<1)){
        System.out.println("Please try again and enter values between 1-6");
      }
    }
    
    ///////if they messed up their preference
    else { //if user preference falls outside 1-2, correct them
      System.out.println("You didn't enter 1 or 2, please try again");
    }
    
    
  }
}