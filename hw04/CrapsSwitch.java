//////// Andrew Fedun CSE 02 Section 110
/////// HW 4 CrapsSwitch
///// September 24 2018

/* This program is designed to either generate random dice values or take user inputs. 
it then generates the corresponding craps slang using switch statements. if improper dice values are entered, it asks the user to try again*/

// I had to do the assignment twice because codeanywhere deleted my first version. Please keep this in mind when grading :)

import java.util.Scanner; //imports scanner
import java.util.Random; //imports random number generator

public class CrapsSwitch{
  public static void main (String[]args){
    Scanner scnr = new Scanner(System.in); //declares scannerner
    Random rand = new Random(); //declares random number generator
    int pref; //declares preference variable
    int d1, d2; //declares variables for dice 1 and dice 2
    
    System.out.println("Please enter either 1 for randomly generated dice or 2 to input your dice"); //promps user to enter preference
    pref = scnr.nextInt(); //saves users preference
    
    switch(pref){
        
      /////user preference for randomly generated dice  
      case 1:
        d1 = rand.nextInt(6) + 1; //generates dice 1 value 1-6
        d2 = rand.nextInt(6) + 1;  //generates dice 2 value 1-6
        
        ///screens the value of the first dice value
        switch(d1){
            
          ////first dice is 1
          case 1:
            
            //screens the value of the second dice value
            switch(d2){
              case 1:
                System.out.println("Snake Eyes");
                break;
              case 2:
                System.out.println("Ace Deuce");
                break;
              case 3:
                System.out.println("Easy Four");
                break;
              case 4:
                System.out.println("Fever Five");
                break;
              case 5:
                System.out.println("Easy Six");
                break;
              case 6:
                System.out.println("Seven Out");
                break;
            }
            break;
            
            //the code becomes repetitive from here on. there will be comments on non-repetitive code
            
            /////first dice is 2
            case 2:
              switch(d2){
                case 1:
                  System.out.println("Ace Deuce");
                  break;
                case 2: 
                  System.out.println("Hard Four");
                  break;
                case 3:
                  System.out.println("Fever Five");
                  break;
                case 4:
                  System.out.println("Easy Six");
                  break;
                case 5:
                  System.out.println("Seven Out");
                case 6:
                  System.out.println("Easy Eight");
                  break;
              }
            break;
            
            /////first dice is 3
            case 3:
              switch (d2){
                case 1:
                  System.out.println("Easy Four");
                  break;
                case 2:
                  System.out.println("Fever Five");
                  break;
                case 3:
                  System.out.println("Hard Six");
                  break;
                case 4:
                  System.out.println("Seven Out");
                  break;
                case 5:
                  System.out.println("Easy Eight");
                  break;
                case 6:
                  System.out.println("Nine");
                  break;
              }
            break;
            
            /////first dice is 4
            case 4:
              switch(d2){
                case 1:
                  System.out.println("Fever Five");
                  break;
                case 2:
                  System.out.println("Easy Six");
                  break;
                case 3:
                  System.out.println("Seven Out");
                  break;
                case 4:
                  System.out.println("Hard Eight");
                  break;
                case 5:
                  System.out.println("Nine");
                  break;
                case 6:
                  System.out.println("Easy Ten");
                  break;
              }
            break;
            ////first dice is 5
          case 5:
            switch(d2){
              case 1:
                System.out.println("Easy Six");
                break;
              case 2:
                System.out.println("Seven Out");
                break;
              case 3:
                System.out.println("Easy Eight");
                break;
              case 4:
                System.out.println("Nine");
                break;
              case 5:
                System.out.println("Hard Ten");
                break;
              case 6:
                System.out.println("Yo-leven");
                break;
            }
            break;
            /////first dice is 6
          case 6:
            switch(d2){
              case 1:
                System.out.println("Seven Out");
                break;
              case 2:
                System.out.println("Easy Eight");
                break;
              case 3:
                System.out.println("Nine");
                break;
              case 4:
                System.out.println("Easy Ten");
                break;
              case 5:
                System.out.println("Yo-leven");
                break;
              case 6:
                System.out.println("Boxcars");
                break;
            }
            }
        break;
        
      ////////user preference to input dice  
      case 2:
        System.out.println("Please enter the number on your first dice"); //prompts user to enter first dice value
        d1 = scnr.nextInt(); //saves dice value
        System.out.println("Please enter the number on your second dice");
        d2 = scnr.nextInt();
        
        //screens val of first dice
        switch(d1){
            
          ////first dice is 1
          case 1:
            
            //screens valueof second dice
            switch(d2){
              case 1:
                System.out.println("Snake Eyes");
                break;
              case 2:
                System.out.println("Ace Deuce");
                break;
              case 3:
                System.out.println("Easy Four");
                break;
              case 4:
                System.out.println("Fever Five");
                break;
              case 5:
                System.out.println("Easy Six");
                break;
              case 6:
                System.out.println("Seven Out");
                break;
              default:
                System.out.println("Please try again, entering dice values between 1-6");
                break;
            }
            break;
            
            
            /////first dice is 2
            case 2:
              switch(d2){
                case 1:
                  System.out.println("Ace Deuce");
                  break;
                case 2: 
                  System.out.println("Hard Four");
                  break;
                case 3:
                  System.out.println("Fever Five");
                  break;
                case 4:
                  System.out.println("Easy Six");
                  break;
                case 5:
                  System.out.println("Seven Out");
                case 6:
                  System.out.println("Easy Eight");
                  break;
                default:
                  System.out.println("Please try again, entering dice values between 1-6");
                  break;
              }
            break;
            
            
            /////first dice is 3
            case 3:
              switch (d2){
                case 1:
                  System.out.println("Easy Four");
                  break;
                case 2:
                  System.out.println("Fever Five");
                  break;
                case 3:
                  System.out.println("Hard Six");
                  break;
                case 4:
                  System.out.println("Seven Out");
                  break;
                case 5:
                  System.out.println("Easy Eight");
                  break;
                case 6:
                  System.out.println("Nine");
                  break;
                default:
                  System.out.println("Please try again, entering dice values between 1-6");
                  break;
              }
            break;
            
            
            /////first dice is 4
            case 4:
              switch(d2){
                case 1:
                  System.out.println("Fever Five");
                  break;
                case 2:
                  System.out.println("Easy Six");
                  break;
                case 3:
                  System.out.println("Seven Out");
                  break;
                case 4:
                  System.out.println("Hard Eight");
                  break;
                case 5:
                  System.out.println("Nine");
                  break;
                case 6:
                  System.out.println("Easy Ten");
                  break;
                default:
                  System.out.println("Please try again, entering dice values between 1-6");
                  break;
              }
            break;
            
            
            ////first dice is 5
          case 5:
            switch(d2){
              case 1:
                System.out.println("Easy Six");
                break;
              case 2:
                System.out.println("Seven Out");
                break;
              case 3:
                System.out.println("Easy Eight");
                break;
              case 4:
                System.out.println("Nine");
                break;
              case 5:
                System.out.println("Hard Ten");
                break;
              case 6:
                System.out.println("Yo-leven");
                break;
              default:
                  System.out.println("Please try again, entering dice values between 1-6");
                  break;
            }
            break;
            
            
            /////first dice is 6
          case 6:
            switch(d2){
              case 1:
                System.out.println("Seven Out");
                break;
              case 2:
                System.out.println("Easy Eight");
                break;
              case 3:
                System.out.println("Nine");
                break;
              case 4:
                System.out.println("Easy Ten");
                break;
              case 5:
                System.out.println("Yo-leven");
                break;
              case 6:
                System.out.println("Boxcars");
                break;
              default:
                  System.out.println("Please try again, entering dice values between 1-6");
                  break;
            }
            break;
            
         default:
            System.out.println("Please try again, entering dice values between 1-6"); //users who entered numbers not 1-6 will be shown this message
            break;
            }
        break;
        
      default:
        System.out.println("Please try again, entering a value either 1 or 2"); //users who entered a preference not 1-2 will be shown this message
            break;
            }
            }
              }
            
<<<<<<< HEAD
=======
              
>>>>>>> e3315621134e0ee356f8ca161611dbff570ad0ba
              