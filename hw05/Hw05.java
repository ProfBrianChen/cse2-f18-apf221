//////// Andrew Fedun HW5 Poker Hand Probability
////// 10/9/18 Arielle Carr CSE 2 
//// This program is designed to take user input of number of hands to test and output the probability for each type of poker hands


import java.util.Scanner; //imports scanner
import java.util.Random; //imports random
import java.text.DecimalFormat; //imports decimal format

public class Hw05 {
public static void main (String[]args){
  Scanner scnr = new Scanner(System.in); //creates scanner
  Random rand = new Random(); //creates random number generator
  DecimalFormat df = new DecimalFormat(".000"); //creates desired decimal format
  
  double hands = 0; //creates variable for number of hands 
  boolean check1 = false; //creates boolean to check for integer
  int j = 1; //creates counter
  int c1 = 0, c2=0, c3=0, c4=0, c5=0; //creates variables for each card  
  double fourook=0; //creates counter for four of a kind 
  double twoook=0; //creates counter for one pair  
  double threeook=0; //creates counter for three of a kind 
  double tpair=0; //creates counter for two pair 
  
  
  
  while (check1 == false){ //screens if user has entered integer 
  System.out.println("Please enter the number of hands you want to test"); //prompts user to enter desired number of hands 
  if (check1 = scnr.hasNextInt()){ //checks that user has properly entered integer 
  hands = scnr.nextInt(); //assigns user input to variable hands 
  }
  else{ //if they didnt enter an integer
    scnr.next(); //clear the scanner entry 
  }
  }
  
  
  while (j <= hands){ //while there are still hands to test 
        c1 = rand.nextInt(51)+1; //creates random card 
        c2 = rand.nextInt(51)+1;//creates random card 
        c3 = rand.nextInt(51)+1;//creates random card 
        c4 = rand.nextInt(51)+1;//creates random card 
        c5 = rand.nextInt(51)+1;//creates random card 
          /*System.out.println("original cards"); //uncomment this to see the original cards printed
          System.out.println(c1);
          System.out.println(c2);
          System.out.println(c3);
          System.out.println(c4);
          System.out.println(c5);*/
      if ((c1 != c2)&&(c1 != c3)&&(c1 != c4)&&(c1 != c5)&&(c2 != c3)&&(c2 != c4)&&(c2 != c5)&&(c3 != c4)&&(c3 != c5)&&(c4 != c5)){         
        if ((c1>=14)&&(c1<=26)){ //checks to see that cards are unique 
          c1 = c1-13; //standardizes card to 1-13 scale
        }
        else if ((c1>=27)&&(c1<=39)){
          c1 = c1-26; //standardizes card to 1-13 scale
        }
        else if ((c1>=40)&&(c1<=52)){
          c1 = c1-39;//standardizes card to 1-13 scale
        }
        if ((c2>=14)&&(c2<=26)){
          c2 = c2-13;//standardizes card to 1-13 scale
        }
        else if ((c2>=27)&&(c2<=39)){
          c2 = c2-26;//standardizes card to 1-13 scale
        }
        else if ((c2>=40)&&(c2<=52)){
          c2 = c2-39;//standardizes card to 1-13 scale
        }
        if ((c3>=14)&&(c3<=26)){
          c3 = c3-13;//standardizes card to 1-13 scale
        }
        else if ((c3>=27)&&(c3<=39)){
          c3 = c3-26;//standardizes card to 1-13 scale
        }
        else if ((c3>=40)&&(c3<=52)){
          c3 = c3-39;//standardizes card to 1-13 scale
        }
        if ((c4>=14)&&(c4<=26)){
          c4 = c4-13;//standardizes card to 1-13 scale
        }
        else if ((c4>=27)&&(c4<=39)){
          c4 = c4-26;//standardizes card to 1-13 scale
        }
        else if ((c4>=40)&&(c4<=52)){
          c4 = c4-39;//standardizes card to 1-13 scale
        }
        if ((c5>=14)&&(c5<=26)){
          c5 = c5-13;//standardizes card to 1-13 scale
        }
        else if ((c5>=27)&&(c5<=39)){
          c5 = c5-26;//standardizes card to 1-13 scale
        }
        else if ((c5>=40)&&(c5<=52)){
          c5 = c5-39;//standardizes card to 1-13 scale
        }
        
          /*System.out.println("standardized cards"); //uncomment this to see the standardized cards printed
          System.out.println(c1);
          System.out.println(c2);
          System.out.println(c3);
          System.out.println(c4);
          System.out.println(c5);*/
          
          
          //if its a four of a kind
           if ((c1==c2)&&(c2==c3)&&(c3==c4) || (c1==c2)&&(c2==c3)&&(c3==c5) || (c1==c2)&&(c2==c4)&&(c4==c5) || (c1==c3)&&(c3==c4)&&(c4==c5) || (c2==c3)&&(c3==c4)&&(c4==c5)){
            ++fourook; //if four of a kind, up the counter 
          }
          //if its a 3 of a kind
          else if (c1==c2&&c2==c3 || c1==c4&&c4==c5 || c3==c4&&c4==c5 || c2==c3&&c3==c4 || c2==c3&&c3==c5 || c2==c4&&c4==c5 || c1==c3&&c3==c5 || c1==c3&&c3==c4 || c1==c2&&c2==c5 || c1==c2&&c2==c4){
            ++threeook; //if three of a kind, up the counter
          }
          //if its a one pair
          else if (c1==c2 || c1==c3 || c1==c4 || c1==c5 || c2==c3 || c2==c4 || c2==c5 || c3==c4 || c3==c5 || c4==c5){
            ++twoook; //if its a pair,up the counter 
          }
          //if its a two pair
          else if ((c1==c2)&&(c3==c4) || (c1==c3)&&(c2==c4) || (c1==c4)&&(c2==c3) || (c1==c2)&&(c3==c5) || (c1==c3)&&(c2==c5) || (c1==c5)&&(c2==c3) || (c1==c2)&&(c4==c5) || (c1==c4)&&(c2==c5) || (c1==c5)&&(c2==c4) || (c1==c3)&&(c4==c5) || (c1==c4)&&(c3==c5) || (c1==c5)&&(c3==c4) || (c2==c3)&&(c4==c5) || (c2==c4)&&(c3==c5) || (c2==c5)&&(c3==c4)){
            ++tpair; //if its a two pair, up the counter 
          }
          
          
          
          
          
        }//closing bracket for if all cards are unique
        
      else{
        --j; //if cards were not unique, dont count this round
      }
  ++j; //up the counter for testing this hand 
  }
  
  /*System.out.println("four: "+fourook);
  System.out.println("three: "+threeook);
  System.out.println("pair: "+twoook);
  System.out.println("twopair: "+tpair);*/
  
   double fourofakind = fourook/hands; //calculate probability
   double onepair = twoook/hands;//calculate probability
   double threeofakind = threeook/hands;//calculate probability
   double twopair = tpair/hands;//calculate probability
  
  System.out.println("The number of loops is : " + (int)hands);
  System.out.println("The probability of Four-of-a-kind: "+ df.format(fourofakind)); //print probability
  System.out.println("The probability of Three-of-a-kind: "+ df.format(threeofakind));//print probability
  System.out.println("The probability of Two-pair: "+ df.format(twopair));//print probability
  System.out.println("The probability of One-pair: "+ df.format(onepair));//print probability
///ending brackets   
}
}