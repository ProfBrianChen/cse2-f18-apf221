//////// Andrew Fedun HW5 Poker Hand Probability
////// 10/23/18 Arielle Carr CSE 2 
//// This program is designed to take user input and print a corresponding encrypted x

import java.util.Scanner;//imports scanner

public class EncryptedX{
  public static void main(String[]args){
		Scanner scnr = new Scanner(System.in);//creates scanner 
		int user = 11;//initiates user input as 11 to force into while loop
		
		while (user<1 || user>10){//run while user hasnt input a good integer 
		
		System.out.println("Please enter an integer from 1-10");//prompt user to input integer 
		user = scnr.nextInt();//save user input 
		    if (user<1 || user>10){//if user didnt input within range of 1-10 
		        System.out.println("You didn't enter an integer from 1-10, please try again");//prompt user to try again
		    }    
		}
		
		
		
		int count = 1; //initiate count at 1 
		for (int j = 1; j<=user; j++){//creates outer for loop to increment up until all rows are filled
		        
		    for (int c = 1; c<=user; c++){//creates inner for loop to increment up until all columns are filled 
		        
		        if(count==c||c==user+1-count){//if the row number councides with the column number and vice versa, print a space
		        System.out.print(" ");//print space 
		        }
		        
		        else{//all other spots get a *
		        System.out.print("*");//print asterix
		        }
		        
		    }
		    System.out.println();//create new line to start next row 
		    
		    count++;//increment counter to signify where spaces belong for upcoming row 
		}
		
		
	}

}


    