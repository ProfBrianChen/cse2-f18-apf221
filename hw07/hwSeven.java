//////Andrew Fedun HW 7 10/30/18
//////Arielle Carr
////This program is designed to use methods to take user text and allow the user to input
////a string and choose from a menu various ways to analyze that string


import java.util.Scanner; //imports scanner


public class hwSeven {
	
	
///method for getting users sample text	
public static String sampleText() {
		Scanner scnr = new Scanner(System.in); //creates scanner
		System.out.println("Enter a sample text:"); //prompts user for sample text
		String sample = scnr.nextLine(); //saves sample text
		System.out.println(); //creates new line for spacing
		return sample; //returns the string sample back to the main method
	}
	

///method for printing the menu
public static String printMenu() {
	Scanner scnr = new Scanner(System.in); //creates scanner
	String m = "error"; //creates string for the users choice
	boolean check = false; //creates boolean to see if user entered valid character
	
	while(check == false) { //runs until user enters valid input
	System.out.println("Menu"); //prints out the menu
	System.out.println("c - Number of non-whitespace characters");
	System.out.println("w - Number of words");
	System.out.println("f - Find text");
	System.out.println("r - Replace all !'s");
	System.out.println("s - Shorten spaces");
	System.out.println("q - Quit");
	System.out.println();
	System.out.println("Choose an option: ");
	m = scnr.next(); //saves users input
		if(m.equals("c") || m.equals("w") || m.equals("f") || m.equals("r") || m.equals("s") || m.equals("q")) {
			check = true;//if user entered valid input, make check true to exit loop
	     }
	    else {
	    	System.out.println("Invalid character entered, please try again"); //if user entered invalid character, prompt for another
	    	check = false;
	    }
	}
	
	return m; //return the users character choice back to main method
}


///method to get non white space characters
public static int getNumOfNonWSCharacters(String samp) {
	System.out.println(); //prints new line for spacing
	String noSpaces = samp.replace(" ",  ""); //creates new string that replaces all of users spaces with non spaces
	int numChar = noSpaces.length(); //creates new integer to save length of the new string with no spaces to get num characters
	
	return numChar; //return the number of characters to the main method
}



///method to get number of words
public static int getNumOfWords(String samp) {
	String [] words = samp.split("\\s+");//splits string at every space made
	int numWords = words.length; //finds number of words now that theyre split up
	return numWords;//returns number of words to the main method
	
}

///method to find text within
public static String findText(String samp) {
	Scanner scnr = new Scanner(System.in); //creates scannner
	int sampLength = samp.length(); //creates integer sample length to get
	//System.out.println("your sample length is: "+sampLength); -- uncomment this to see sampLength 
	System.out.println("Enter a word or phrase to be found: ");  //prompt user for a word they want to find
	String find = scnr.nextLine(); //save users input
	int findLength = find.length(); //create integer for the length of users input
	//System.out.println("your find length is: " + findLength);
	int counter = 0; //create counter for number of instances of that word
	for (int i=0; i<sampLength; i++) { //run loop to go through every character in the sample
		for(int j=0; j<findLength; j++) { //run loop to go through every character in the users input
			if(find.charAt(j) != samp.charAt(i+j)) { //if the character at a position in find doesnt equal char at the corresponding position in samp, the word is not there
				break; 
			}
			else { //if the characters do match
				if(j+1 == findLength) { //and if the number of matching letters match the number of letters in the users input
					counter++; //increase counter of instances of the users input
					break; 
				}
			}
		}
	}
	String state = "The word/phrase [" + find+ "] showed up " + counter + " times"; //formulate string to deliver answer to user
	return state; //return string to main method
}


///method for replacing all exclamation points
public static String replaceExclamation(String samp) {
	samp = samp.replace('!',  '.'); //replace all ! with . in samp
	return samp; //return edited samp to main method
}


///method for shortening spaces
public static String shortenSpace(String samp) {
	samp = samp.replaceAll("\\s+",  " "); //replace all 2 or more spaces with a single space
	return samp; //return edited string to main method
}


///main method
public static void main (String[]args) {
	String sample = sampleText(); //call sampleText method to get a string from user
	System.out.println("You entered: " + sample); //tell user what they entered
	System.out.println(); //print line for spacing
	String m = printMenu(); //call menu printing method
	System.out.println("You picked: " + m); //tell user what they picked
	
	
	if(m.equals("q")) { //tell user they quit
		System.out.println("You have quit the program, have a nice day!");
	}
	
	if(m.equals("c")) { //call the number of non white space characters method
	int numChar = getNumOfNonWSCharacters(sample); //save the return as an int
	System.out.println("Number of non-whitespace characters: "+numChar); 
	}
	
	else if(m.equals("w")) { //call num of words method
	int numWords = getNumOfWords(sample); //save the return as int
	System.out.println("Number of words: " +numWords);
	}
	
	else if(m.equals("f")) { //call find text method
	String numText = findText(sample); //save return as a string
	System.out.println(numText);
	}
	
	else if(m.equals("r")) { //call replace exclamation method
		String newString = replaceExclamation(sample); //save return as a string
		System.out.println("Edited text: "+newString);
	}
	
	else if(m.equals("s")) { //call shorten space method
		String newlit = shortenSpace(sample); //save return as a string
		System.out.println("Edited text: "+newlit);
	}

	
}

}
