
////Andrew Fedun 11/27/18 HW 9 Part 1
////CSE 2 Arielle Carr
///This program is designed to take in 15 ascending grades and use binary and linear search to find targets
import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
	
/////print method	
	public static void print(int[] prArr) {
		for(int y=0; y<prArr.length; y++) //run for every element of the array
			System.out.print(prArr[y] +" ");
	}
	
////binary search method
	public static int binarySearch(int[] ogArr, int findInt) {
		int arrLength = ogArr.length;//save array length
		int mid;//create variable for middle
		int low = 0;//create variable for low
		int high = arrLength - 1;//create variable for high end
		int counter = 1;//create counter
		
		while(high>=low) {
			mid = (high+low) / 2;//make mid the halfway point between high & low
			if (ogArr[mid]<findInt) {//if mid is less than the target,
				low = mid+1;//make mid +1 the new low
			}
			else if (ogArr[mid]>findInt) {//if mid is greater tan the target
				high = mid-1;//make mid -1 the new high 
			}
			else {//otherwise, that means that mid is the target
				return counter; //return the position of the element
			}			
		counter = counter +1;//increase counter;
		}
		return -1;//if target isnt found, return -1
		
	}
	
	
////scrambler method 
	public static int[] scrambler(int[]ogArr) {
		Random rand = new Random();	 //create random number generator
		for(int j = 0; j<80;j++) { //run 80 times to scramble
			int k =rand.nextInt(15); //pick a random number 0-15
			int temp; //create temp value
			temp = ogArr[0]; //save first element to temp
			ogArr[0] = ogArr[k]; //set first element to the random element
			ogArr[k] = temp; //set random element to the original value of the first
	}
		return ogArr;//return scrambled array
	}
	

	
////linear search
	public static int linearSearch(int[] ogArr, int findInt) {//take in array and target
		
		int counter = 1;//create counter
		for (int i=0; i< ogArr.length; ++i) {//run for every element in the array
			if (ogArr[i] == findInt) {//if the element equals the target
				return counter;//return the iterations
			}
			counter = counter+1;//increment counter
			
		}
		return -1;//if not found, return -1
	}
	
	
////main method	
	public static void main (String[]args) {
	Scanner scnr = new Scanner(System.in);//create scanner
	int[] grades = new int[15];//create array for grades
	int temp = 101;//create temp value to hold individual grades
	boolean check = false;//create boolean for checking integers later
	
	
	System.out.println("enter 15 ascending ints for final grades in CSE2: ");//prompt user for grades
	
	
for(int i=0; i<15; i++) {//run for every element of the array
	
	
	while (check==false) {//run while theres no integer inputted
		check = scnr.hasNextInt();//check if theres an integer
		if (check = scnr.hasNextInt()) {//if there is an integer
			temp = scnr.nextInt();//save integer to temp
			
			while(temp<0 || temp>100) {//whil int is out of range
				System.out.println("not within the range of 0-100, enter new: ");//ask user to fix it
				temp = scnr.nextInt();//save their new try
			}
				
			grades[i] = temp;	//set grade into element
			if(i>0) {//run starting the second element
			while(grades[i]<grades[i-1]) {//while its not ascending
				System.out.println("entered grade was not ascending, enter new: ");//ask user to fix it
				grades[i]=scnr.nextInt();//save new try
			}
			}	
		}
		

		else {
			scnr.next();//clear scanner
			System.out.println("not an integer");//tell user not an int
		}		
	}
	check=false;//keep check false if no int
}	
	
	
print(grades);//print grades
System.out.println();
System.out.println("Enter a grade to search for");
int findInt = scnr.nextInt();//save target

int binCheck = binarySearch(grades, findInt);//call bincheck
if (binCheck==-1) {//if returns -1, target wasnt found
	System.out.println(findInt+" was not found in the list with 4 iterations");//max iterations is 4
}
else {
	System.out.println("Found " + findInt+ " in "+binCheck+" iterations.");//found it in returned iterations
}

int[] scrambled = scrambler(grades);//call scramble
System.out.println("Scrambled: ");
print(scrambled);


System.out.println("Enter a grade to search for");
int linFind = scnr.nextInt();//save target
int linCheck = linearSearch(grades, linFind);//call linsearch
if(linCheck==-1) {
	System.out.println(linFind + " was not found in the list with 15 iterations.");//if -1 returned, didnt find
}
else {
	System.out.println("Found " + linFind+ " in "+linCheck+ " iterations.");//found in returned iterations
}




}
}