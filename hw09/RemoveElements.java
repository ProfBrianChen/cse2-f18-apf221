
////Andrew Fedun 11/27/18 HW 9 Part 2
////CSE 2 Arielle Carr
///This program is designed to generate a random array, remove a certain element, and remove all elements equalling a target
import java.util.Random;//import random
import java.util.Scanner;//import scanner
public class RemoveElements {

	
////random input method
	public static int[] randomInput() {
		int[] arr = new int[10];//create array of 10
		Random rand = new Random();//create random
		for (int i=0; i<arr.length; i++ ) {//run for every element in the array
			arr[i] = rand.nextInt(9);//set element to a random digit
		}
		return arr;//return digit			
	}	
	
	
////delete method
	public static int[] delete(int[] list, int pos) {//input list and position
		int[] newlist = new int[list.length - 1];//make new list one less element than the original
		for(int i=0; i<newlist.length; i++) {//run for every element in the new list
			if(i>=pos) {//if the element is equal to or greater than the position were deleting
				newlist[i]=list[i+1];//pull from the element preceding
			}
			else {
				newlist[i]=list[i];//otherwise, keep the same element
			}
		}
		return newlist;//return the new list
	}
	
	

////remove method
	public static int[] remove(int[] list, int target) {//input the list and target value	
	int counter = 0;//create counter
	for(int k =0; k<list.length; k++) {//run for every element in the array
		if (list[k]==target) {//if the element equals the target value
			
			counter = counter+1;//increase the counter
			//System.out.println("counter: " +counter);
			
		}
	}
	
	
	int[] newlist = new int[list.length-1-counter]; //create new list the length of the original - the instances of the target
	for(int g=0; g<newlist.length; g++) {//run for every element in the array
		newlist[g]=list[g];//set new list elements to list elements
	}
	
	
	
	for(int j=0; j<newlist.length; j++) {//run for every element of new list
		
	
	 if(list[j] == target) {//if the element = target
			for(j=j ; j<newlist.length - 1; j++) {//pull from the preceding element for every element following the target
			
				newlist[j] = list[j+1];
			}
			
		System.out.println();
				
		
		}
	}
	return newlist;//return the new list
	}
	
	
////main method	
	public static void main (String[]args) {
	Scanner scan=new Scanner(System.in);//creates scanner
	int num[]=new int[10];//creates array of length 10
	int newArray1[];//creates newarray1
	int newArray2[];//creates newarray2
	int index,target;//creates values for index and target
		String answer="";//creates string for the answer
		do{
	  	System.out.print("Random input 10 ints [0-9]");
	  	num = randomInput();//calls random input method to generate 10 integers
	  	String out = "The original array is:";
	  	out += listArray(num); //returns string of form
	  	System.out.println(out);//prints array
	 
	  	System.out.print("Enter the index ");
	  	index = scan.nextInt();//saves the index
	  	newArray1 = delete(num,index);//calls delete method
	  	String out1="The output array is ";
	  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out1);
	 
	      System.out.print("Enter the target value ");
	  	target = scan.nextInt();//saves target value
	  	newArray2 = remove(num,target);//calls remove method
	  	String out2="The output array is ";
	  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out2);
	  	 
	  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
	  	answer=scan.next();//saves user preference
		}while(answer.equals("Y") || answer.equals("y"));//continues running until user enters something other than y
	  }
	 
	  public static String listArray(int num[]){//prints array nicely
		String out="{";
		for(int j=0;j<num.length;j++){//runs for every element
	  	if(j>0){
	    	out+=", ";//put comma if intermediate value
	  	}
	  	out+=num[j];
		}
		out+="} ";//prints bracket at end
		return out;//returns formatted array
	  }
	}

	

