// Andrew Fedun, 9/7/18, CSE 2 Arielle Carr section 110
//This  Program is a Cyclometer 
//It will record time elapsed in seconds and rotations of the front wheel during that time
//It takes this information and prints minutes for each trip, number of counts for each trip,
//distance of each trip in miles, and distance for the two trips combined
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
       	int secsTrip1=480;  // declares time elapsed for trip 1 in seconds
       	int secsTrip2=3220;  //declares time elapsed for trip in seconds
	      int countsTrip1=1561;  //declares time elapsed for trip in seconds
		    int countsTrip2=9037; //declares time elapsed for trip in seconds
      
      
        double wheelDiameter=27.0,  //declares the diameter of the wheel in inches
      	PI=3.14159, //declares pi so we can calculate circumference
      	feetPerMile=5280,  // declares conversion rate for feet per mile for calculation
      	inchesPerFoot=12,   //declares conversion rate for inches per foot for calculation
      	secondsPerMinute=60;  //declares conversion rate for seconds per minute for calculation
      	double distanceTrip1, distanceTrip2,totalDistance;  //declares trip variables as doubles for precise calculation of distance later on
      
        System.out.println("Trip 1 took "+
        (secsTrip1/secondsPerMinute)+" minutes and had "+
        countsTrip1+" counts."); //prints out statement regarding trip one's time elapsed and counts
	      System.out.println("Trip 2 took "+
        (secsTrip2/secondsPerMinute)+" minutes and had "+
       	countsTrip2+" counts."); //prints out statement regarding trip one's time elapsed and counts
      
      //this calculation will calculate distance of trip in inches by multiplying counts by wheel diameter by pi
      //it will save this value under distanceTrip1 
	  
      	distanceTrip1=countsTrip1*wheelDiameter*PI; //calculation of distanceTrip1 in inches
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
      
    	distanceTrip1/=inchesPerFoot*feetPerMile; // updates distanceTrip1 from inches to miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //calculates distance of trip 2 directly into miles
	    totalDistance=distanceTrip1+distanceTrip2; //calculates total distance by adding distance of trip 1 and 2
      
      
      
    	//Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints the distance information of trip 1 in miles
	    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints the distance information of trip 2 in miles
	    System.out.println("The total distance was "+totalDistance+" miles"); //prints the distance information of the total trip in miles


	}  //end of main method   
} //end of class

