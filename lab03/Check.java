import java.util.Scanner; //imports the scanner so we can use it later on

///// Andrew Fedun CSE 2 Section 110
//// 9/14/18 Lab 3
/// This program is designed to take user input of a check amount, tip amount, number of friends, and output the evenly split price
//
public class Check{
  //main method required for every Java program
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner ( System.in ); //declares myScanner to be used later on
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //prompts the user to enter the original cost of the check
    double checkCost = myScanner.nextDouble(); //saves the user-entered cost of the check as a double
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompts the user to enter the percentage tip they would like to pay
    double tipPercent = myScanner.nextDouble(); //saves the user-entered tip preference as a double
    tipPercent /=100; //in order to convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //prompts the user to enter number of people out to dinner
    int numPeople = myScanner.nextInt(); //saves the user-entered number of people as an int
    
    double totalCost; //declares variable for the total cost to be later calculated
    double costPerPerson; //declares variable for the cost per person
    int dollars,   //whole dollar amount of cost 
    dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
    totalCost = checkCost * (1 + tipPercent); //calculates the total cost by multiplying for total tip and adding to check cost
    costPerPerson = totalCost / numPeople; //calculates cost per person by dividing total cost by number of people
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson; //gets the dollar amount with no decimals
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10; //gets the tens place of the cost per person
    pennies=(int)(costPerPerson * 100) % 10; //gets the hundreths place of cost per person
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //prints out the amount each person owes


    
  } //end of main method
} // end of class