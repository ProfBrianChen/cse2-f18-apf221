///// Andrew Fedun CSE 2 Section 110
//// 9/21/18 Lab 3
/// This program is designed to randomly generate a card 1-52 and print out its name and suit
//

import java.util.Random; //imports the random utility so we can generate a random number

public class CardGenerator{ 
  public static void main (String[]args){
    Random rand = new Random(); //creates a random number generator rand
    int card = rand.nextInt(52) +1 ; //randomly generates a card number 1-52
    
    String suit, id; //declares variables for the suit and name of the card
    System.out.print("You picked up the "); //prints out first half of the statement to the user
    
    if ((card>=1) && (card<=13))  { //if statement to screen for cards 1-13 that are diamonds
      suit = "Diamonds"; //labels these cards as diamonds
      
      switch(card){ //switch statement to screen card number to name the cards
        case 1: //identifies if the card is number 1
          id = "Ace"; //labels card as ace
          System.out.print(id); //prints card name into statement
          break; // break to go to next case
        case 2:
          id = "2";
          System.out.print(id);
          break;
        case 3:
          id = "3";
          System.out.print(id);
          break;
        case 4:
          id = "4";
          System.out.print(id);
          break;
        case 5:
          id = "5";
          System.out.print(id);
          break;
        case 6:
          id = "6";
          System.out.print(id);
          break;
        case 7:
          id = "7";
          System.out.print(id);
          break;
        case 8:
          id = "8";
          System.out.print(id);
          break;
        case 9:
          id = "9";
          System.out.print(id);
          break;
        case 10:
          id = "10";
          System.out.print(id);
          break;
        case 11:
          id = "Jack";
          System.out.print(id);
          break;
        case 12:
          id = "Queen";
          System.out.print(id);
          break;
        case 13:
          id = "King";
          System.out.print(id);
          break;
        
      }
        
      System.out.println( " of "+suit); //finishes statement to user by append "of whatever suit the card is - in this case diamonds
    }
    
    /////
    else if ((card>=14) && (card<=26)) { //if statement to screen for cards 14-26 that are clubs
      suit = "Clubs";
      card = card - 13; //adjusts cards to a 1-13 scale to make the switch statement easier
       switch(card){
        case 1:
          id = "Ace";
          System.out.print(id);
          break;
        case 2:
          id = "2";
          System.out.print(id);
          break;
        case 3:
          id = "3";
          System.out.print(id);
          break;
        case 4:
          id = "4";
          System.out.print(id);
          break;
        case 5:
          id = "5";
          System.out.print(id);
          break;
        case 6:
          id = "6";
          System.out.print(id);
          break;
        case 7:
          id = "7";
          System.out.print(id);
          break;
        case 8:
          id = "8";
          System.out.print(id);
          break;
        case 9:
          id = "9";
          System.out.print(id);
          break;
        case 10:
          id = "10";
          System.out.print(id);
          break;
        case 11:
          id = "Jack";
          System.out.print(id);
          break;
        case 12:
          id = "Queen";
          System.out.print(id);
          break;
        case 13:
          id = "King";
          System.out.print(id);
          break;
        
      }
        
      System.out.println( " of "+suit);
    }
      
    
    else if((card>=27) && (card<=39)) { //if statement to screen for cards 27-39 that are diamonds
      suit = "Hearts";
      card = card-26; //adjusts cards to a 1-13 scale to make the switch statement easier
      
      switch(card){
        case 1:
          id = "Ace";
          System.out.print(id);
          break;
        case 2:
          id = "2";
          System.out.print(id);
          break;
        case 3:
          id = "3";
          System.out.print(id);
          break;
        case 4:
          id = "4";
          System.out.print(id);
          break;
        case 5:
          id = "5";
          System.out.print(id);
          break;
        case 6:
          id = "6";
          System.out.print(id);
          break;
        case 7:
          id = "7";
          System.out.print(id);
          break;
        case 8:
          id = "8";
          System.out.print(id);
          break;
        case 9:
          id = "9";
          System.out.print(id);
          break;
        case 10:
          id = "10";
          System.out.print(id);
          break;
        case 11:
          id = "Jack";
          System.out.print(id);
          break;
        case 12:
          id = "Queen";
          System.out.print(id);
          break;
        case 13:
          id = "King";
          System.out.print(id);
          break;
        
      }
        
      System.out.println( " of "+suit);
      
    }
    else if((card>=40) && (card<=52)) { //if statement to screen for cards 40-52 that are spades
      suit = "Spades";
      card = card - 39; //adjusts cards to a 1-13 scale to make the switch statement easier
      
      switch(card){
        case 1:
          id = "Ace";
          System.out.print(id);
          break;
        case 2:
          id = "2";
          System.out.print(id);
          break;
        case 3:
          id = "3";
          System.out.print(id);
          break;
        case 4:
          id = "4";
          System.out.print(id);
          break;
        case 5:
          id = "5";
          System.out.print(id);
          break;
        case 6:
          id = "6";
          System.out.print(id);
          break;
        case 7:
          id = "7";
          System.out.print(id);
          break;
        case 8:
          id = "8";
          System.out.print(id);
          break;
        case 9:
          id = "9";
          System.out.print(id);
          break;
        case 10:
          id = "10";
          System.out.print(id);
          break;
        case 11:
          id = "Jack";
          System.out.print(id);
          break;
        case 12:
          id = "Queen";
          System.out.print(id);
          break;
        case 13:
          id = "King";
          System.out.print(id);
          break;
        
      }
        
      System.out.println( " of "+suit);
    
    }
    
    
    
    
  }
}