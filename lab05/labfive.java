///// Andrew Fedun CSE 2 Section 110
//// 10/5/18 Lab 3
/// This program is designed to take inputs from a user about their class info, and only accept data that fits the type
//

import java.util.Scanner; //imports scanner

public class labfive{
  public static void main (String[]args){
    
    
    Scanner scnr = new Scanner(System.in); //creates scanner
    int coursenum = 0; //creates variable for course number, sets to zero to help debug
    String deptname = "error"; //creates string for department name , sets to zero to help debug
    int meetnum = 0; //creates int for meeting times, sets to zero to help debug
    int classtime = 0; //creates variable
    String instrname = "error";//creates variable ""
    int numstud = 0; //creates variable ""
    boolean check1 = false; //creates boolean to check if the correct types are entered later on
    boolean check2 = false; //creates boolean to check if the correct types are entered later on
    boolean check3 = false; //creates boolean to check if the correct types are entered later on
    boolean check4 = false; //creates boolean to check if the correct types are entered later on
    boolean check5 = false; //creates boolean to check if the correct types are entered later on
    boolean check6 = false; //creates boolean to check if the correct types are entered later on
    
    //course number aye
    while (check1 == false){ //creates while loop that runs if check1 is false
      System.out.println("Please enter your course number"); //prompts user to enter their course number
      if (check1 = scnr.hasNextInt()){ //checks that the user actually entered an integer
        coursenum = scnr.nextInt(); //saves their integer as coursenum
      }
      else{ //if the user didnt enter an integer
        scnr.next(); //clear the scanner memory, and allow while to run again
      }
      
    }
    
    //department name aye
    while (check2 == false){
      System.out.println("Please enter your department name");
      if (check2 = scnr.hasNext()){ //checks for the right type 
        deptname = scnr.next(); //saves user input
      }
      else{
        scnr.next();
      }
      }
    
    //number of meeting times aye
    while (check3 == false){
      System.out.println("Please enter the number of times the class meets a week");
      if (check3 = scnr.hasNextInt()){ //checks for the right type
        meetnum = scnr.nextInt(); //saves user input
      }
      else{
        scnr.next();
      }
    }
    
    //class time aye
    while (check4==false){
      System.out.println("Please enter the nearest hour of the class time in military time");
      if (check4 = scnr.hasNextInt()){ //checks for the right type
        classtime = scnr.nextInt(); //saves user input
      }
      else{
        scnr.next();
      }
    }
    
    //instructor name aye
    while (check5 == false){
      System.out.println("Please enter the name of your instructor");
      if (check5 = scnr.hasNext()){ //checks for the right type
        instrname = scnr.next(); //saves user input
      }
      else{
        scnr.next();
      }
    }
    
    //number of students aye
    while (check6 == false){
      System.out.println("Please enter the number of students in your class");
      if (check6 = scnr.hasNextInt()){ //checks for the right type
        numstud = scnr.nextInt(); //saves user input
      }
      else{
        scnr.next();
      }
      }
    
    
    System.out.println("Your course number is: " + coursenum); //prints out user information
    System.out.println("Your department name is: " + deptname);
    System.out.println("Your number of weekly meetings is: " + meetnum);
    System.out.println("Your class time is: " + classtime);
    System.out.println("Your instructor name is: " + instrname);
    System.out.println("Your class size is: " + numstud);
    }
    
  }
