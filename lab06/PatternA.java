////Andrew Fedun, Lab06, 10/12/18
//This program is designed to use nested loops to create a certain pattern

import java.util.Scanner; //imports scanner
public class PatternA{
	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in); //creates scanner 
		int rows = 11; //initiates rows at 11 so forcing the while statement to run since its not within 1-10
		
		while (rows<1 || rows>10){ //keeps running loop until user inputs int 1-10 
		
		System.out.println("Please enter an integer from 1-10"); //prompts user for int 1-10 
		rows = scnr.nextInt(); //stores user input as rows  
		    if (rows<1 || rows>11){ //if user entered integer outside 1-10 
		        System.out.println("You didn't enter an integer from 1-10, please try again"); //explain user they need to input integer 1-10 
		    }    
		}
		
		
		for (int j = 1; j<=rows; j++){ //creates for loop that increases until number of rows is reached 
		    for (int y = 1; y <=j; y++){ //creates for loop that prints increasing integers corresponding to row were in 
		        System.out.print(y); //prints the integer 
		    }
		    System.out.println(); //moves to next line to start the next row 
		}
		
		
	}

}
