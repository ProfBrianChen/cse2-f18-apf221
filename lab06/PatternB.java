////Andrew Fedun, Lab06, 10/12/18
//This program is designed to use nested loops to create a certain pattern

import java.util.Scanner; //imports scanner 
public class PatternB{
  
  	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in); //creates scanner 
		int rows = 11; //initiates rows as 11, forcing us into while loop 
		
		while (rows<1 || rows>10){ //creates while loop that runs until user inputs integer 1-10 
		
		System.out.println("Please enter an integer from 1-10"); //prompts user for int 1-10 
		rows = scnr.nextInt(); //saves user input 
		    if (rows<1 || rows>11){ //if user didnt input int 1-10 
		        System.out.println("You didn't enter an integer from 1-10, please try again"); //ask user to try again
		    }    
		}
		
		
		
		
		for (int j = 1; j<=rows; j++){ //creates forloop to create rows 
		    for (int y = 1; y <=rows+1-j; y++){ //creates for loop to print out a decreasing amount of integers
		        System.out.print(y); //prints integers 
		    }
		    System.out.println(); //starts next line
		}
		
		
	}

}
  
  
  
  
  
