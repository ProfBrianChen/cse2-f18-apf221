////Andrew Fedun, Lab06, 10/12/18
//This program is designed to use nested loops to create a certain pattern

import java.util.Scanner; //imports scanner 
public class PatternC{ 
  
  public static void main (String[]args){


Scanner scnr = new Scanner(System.in); //creates scanner 
        int rows = 11; //initiates rows as 11, forcing into while loop 
        
        
        while (rows<1 || rows>10){ //creates while loop that runs until user inputs int 1-10 
        
        System.out.println("Please enter an integer from 1-10"); //prompts user for int 1-10 
        rows = scnr.nextInt();//saves user input 
            if (rows<1 || rows>11){//if user didnt input an int 1-10 
                System.out.println("You didn't enter an integer from 1-10, please try again"); //asks user to try again 
            }    
        }

    for (int i=1; i<=rows; i++){ //creates for loop to create rows 
        for (int y=0; y<rows-i; y++){ //prints blank spaces in decreasing amounts 
            System.out.print(" ");
        }
        for (int j=i; j>=1; j--){ //prints the integers in decreasing amounts 
            System.out.print(j);   
        }
        
      System.out.println();
    }
    


}


}