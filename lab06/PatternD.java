////Andrew Fedun, Lab06, 10/12/18
//This program is designed to use nested loops to create a certain pattern

import java.util.Scanner; //imports scanner 
public class PatternD{
  
  public static void main (String[]args){


Scanner scnr = new Scanner(System.in); //creates scanner 
        int rows = 11; //initiates rows as 11 to force into while 
        
        while (rows<1 || rows>10){ //runs until user enters int 1-10 
        
        System.out.println("Please enter an integer from 1-10"); //prompts user for int 1-10 
        rows = scnr.nextInt();//saves user input 
            if (rows<1 || rows>11){//if user didnt input int 1-10 
                System.out.println("You didn't enter an integer from 1-10, please try again"); //ask user to try again 
            }    
        }

    for (int i=1; i<=rows; i++){ //creates for loop that makes rows 
        for (int j=rows+1-i; j>0; j--){ //creates for loop tat prints decreasing integers 
            System.out.print(j);
            
            
        }
        
      System.out.println();//starts new line
    }
    


}
}
