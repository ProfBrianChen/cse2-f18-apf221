/////Andrew Fedun Lab 7 10/26
////Arielle Carr
///this program is designed to use methods to create random sentences and a follow up sentences


import java.util.Random; //import random number generator
import java.util.Scanner; //import scanner

public class labSeven {

////adjectives
public static void printAdj(){
    Random rand = new Random();//create random generator
    
    int x = rand.nextInt(9)+1; //generate 1-10
    String adj = "error";//create string for adjective
    
    switch(x){
        case 1://if random number is 1, adjective is awesome
            adj = "awesome ";
            break;
        case 2:
            adj = "important ";
            break;
        case 3:
            adj = "shiny ";
            break;
        case 4:
            adj = "famous ";
            break;
        case 5:
            adj = "cool ";
            break;
        case 6:
            adj = "little ";
            break;
        case 7:
            adj = "fancy ";
            break;
        case 8:
            adj = "weird ";
            break;
        case 9:
            adj = "silly ";
            break;
        case 10:
            adj = "break ";
            break;
        }    
               
   System.out.print(adj);    //print adjective      
}


/////primary noun
public static void printPrimaryNoun(){
    Random rand = new Random();
    int y = rand.nextInt(9)+1;
    String pnoun = "error";
    
    switch(y){
        case 1:
            pnoun = "car ";
            break;
        case 2:
            pnoun = "motorcycle ";
            break;
        case 3:
            pnoun = "boat ";
            break;
        case 4:
            pnoun = "helicopter ";
            break;
        case 5:
            pnoun = "plane ";
            break;
        case 6:
            pnoun = "bike ";
            break;
        case 7:
            pnoun = "ATV ";
            break;
        case 8:
            pnoun = "go kart ";
            break;
        case 9:
            pnoun = "forklift ";
            break;
        case 10:
            pnoun = "skateboard ";
            break;
        }
    System.out.print(pnoun);
}   

////verb
public static void printVerb(){
    Random rand = new Random();
    int m = rand.nextInt(9) + 1;
    String verb = "error";
    
    switch(m){
        case 1:
            verb = "broke ";
            break;
        case 2:
            verb = "shattered ";
            break;
        case 3:
            verb = "hit ";
            break;
        case 4:
            verb = "moved ";
            break;
        case 5:
            verb = "pushed ";
            break;
        case 6:
            verb = "demolished ";
            break;
        case 7:
            verb = "saved ";
            break;
        case 8:
            verb = "revolutionized ";
            break;
        case 9:
            verb = "stopped ";
            break;
        case 10:
            verb = "carried ";
            break;
        }    
     System.out.print(verb );
}


///object noun
public static void printObj(){
    Random rand = new Random();
    int p = rand.nextInt(9)+1;
    String obj = "error";
    
    switch(p){
        case 1:
            obj = "window ";
            break;
        case 2:
            obj = "tree ";
            break;
        case 3:
            obj = "computer ";
            break;
        case 4:
            obj = "building ";
            break;
        case 5:
            obj = "pole ";
            break;
        case 6:
            obj = "umbrella ";
            break;
        case 7:
            obj = "bottle ";
            break;
        case 8:
            obj = "iPhone ";
            break;
        case 9:
            obj = "television ";
            break;
        case 10:
            obj = "calculator ";
            break;
        }
      System.out.print(obj);
}             


///sentence
public static void printSent(){
    System.out.print("Phase 1: ");//print phase 1 when performing phase 1
    System.out.print("The ");
    printAdj();//call corresponding methods
    printPrimaryNoun();
    printVerb();
    System.out.print("the ");
    printObj();
}    
      
            
public static void para(){ 
    System.out.print("Phase 2: ");//phase 2
    System.out.print("It ");
    printVerb();
    System.out.print("the ");
    printObj();
    }




///main 
public static void main (String[]args){
    Scanner scnr = new Scanner(System.in);//create scanner

    int i = 1;//create i as 1
    while (i == 1){//run until user enters 0
    printSent();//method for printing sentence
    
    System.out.println(" ");
    System.out.println("If you want another sentence, please enter 1, otherwise enter 0");
    i = scnr.nextInt(); //save user preference
    }
    
    para();//print paragraph once they exit sentence
    
    }
}