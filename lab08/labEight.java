/////Andrew Fedun Lab 8 11/9/18
////Arielle Carr
///this program is designed to use arrays to count instances of randomly generated integers


import java.util.Arrays; //import arrays
import java.util.Random; //import random

public class labEight {
	public static void main (String[]args) {
	int[] myInts = new int[101]; //create array to hold integers
	int[] numberOcc = new int[101]; //create array to hold number of occurrences
	Random rand = new Random(); //create random number generator
	
	for(int i=0; i<=99; i++) { //run for loop from 0-99
		myInts[i]= rand.nextInt(99); //assign random number to each array item 
		
		for (int j = 0; j<=99; j++) { //run loop from 0-99
		if (myInts[i] == j) { //if the random array number equals the integer at hand
			numberOcc[j]++ ; //increase counter of that integer
		}
		}
		
	}
	
for (int k = 0; k<=99; k++) { //run loop from 0-99
	System.out.println(k + " occurs " + numberOcc[k]+" times"); //print out how many times each integer occurs
}
	
	
	

	
}
}