////Andrew Fedun, 11/23/18, Arielle Carr, Section 110
///This program is designed to manipulate arrays, copying them and inverting them using methods



public class lab09 {

////copy	
	public static int[] copy(int[] ogArr) {
		int[] newArr = new int[ogArr.length];//creates new array the same length as the original
		for (int k=0; k<ogArr.length; k++) {//run for every member of the array
			newArr[k] = ogArr[k];//assign the value from the original to the copy
		}		
		return newArr; //return the copy
	}
	
////inverter
	public static void inverter(int[] ogArr) {
		for(int m=0; m<ogArr.length; m++) { //run for every member of the array
			ogArr[m] = ogArr[(ogArr.length-m)-1]; //swap the member for its opposing value on the other side of the array
		}	
	}
	
////inverter 2
	public static int[] inverter2(int[] ogArr) {
		int[] copyArr = copy(ogArr); //create a copy of the array
		int[] copyInvArr = new int[copyArr.length]; //create an array to store inverted copy
		for(int m=0; m<copyArr.length; m++) { //run for every member of the array
			copyInvArr[m] = copyArr[(copyArr.length-m)-1]; //swap the member for its opposing value on the other side of the array
		}
		return copyInvArr;//return the copied inverted array
	}
	
	
	
////printer
	public static void print(int[] ogArr) {
		for(int w=0; w<ogArr.length; w++) {//run for every member of the array
			System.out.print(ogArr[w]+" "); //print the value with a space
		}
	}
	
	
	
	public static void main (String[]args) {
		
		////part 0 original array
		int[] array0 = new int[15]; //create new array with 15 spots		
		for(int i=0; i<array0.length; i++) { //run for every member of the array
			array0[i]= i; //assign ascending values
    	}
		System.out.print("array0: ");
		print(array0);
					
		
		
		////copy 1 - array1
		System.out.println();
		int[] array1 = copy(array0);//create array1 by calling the copy method for array0
		System.out.print("array1: ");
		print(array1); 
		

		////copy 2 - array2
		System.out.println();
		int[] array2 = copy(array0); //create array2 by calling the copy method for array0
		System.out.print("array2: ");
		print(array2); 
		
		
		////inverting array0
		System.out.println();
		inverter(array0);//call inverter method for array0
		System.out.print("new array0: ");
		print(array0);
		
		
		////inverting array1
		System.out.println();
		inverter2(array1); //call inverter2 method for array1
		System.out.print("new array1: ");
		print(array1);
		
		
		////inverting array2
		System.out.println();
		int[] array3 = inverter2(array2); //create array 3 by assigning it the inverter2 output of array2
		System.out.print("new array3: ");
		print(array3);
		
	
}
}