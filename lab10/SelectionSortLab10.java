
////Andrew Fedun Lab 10 pt 1 Selection Sort
///CSE2 Arielle Carr 12/7/18
///this program is designed to perform selection sort
import java.util.Arrays;

public class SelectionSortLab10 {
	public static void main (String[]args) {
		int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
		int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		
		
		System.out.println("The total number of operations performed on the sorted array: "+iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: "+iterWorst);		
	}
	
	
	public static int selectionSort (int[] list) {
		//Prints the initial array (you must insert another
		//print out statement later in the code to show the array as it's being sorted
		System.out.println(Arrays.toString(list));
		//initialize counter for iterations
		int iterations = 0;
		int temp;
		for (int i=0; i<list.length-1; i++) {
			//update the iterations counter
			iterations++;
			
			//Step One: Find the minimum in the list[i..list.length-1]
			int currentMin = list[i];
			int currentMinIndex = i;
			for (int j=i+1; j<list.length; j++) {
				//COMPLETE THIS FOR LOOP
				if(list[j]<list[currentMinIndex]) {
					currentMinIndex=j;
				}
				
				//in this step, make sure you update the iteration
				//counter time you compare the current min to another element in the array
			}
			
			//Step two: swap list[i] with the minimum you found above
			if(currentMinIndex!=i) {
				//COMPLETE THE CODE TO MAKE THE SWAP
				temp = list[i];
				list[i]=list[currentMinIndex];
				list[currentMinIndex]=temp;
				
				
				
			}
			
			System.out.println(Arrays.toString(list));
			
		}
		return iterations;
			
			
			
		
		
		
		
	}
	
	
	

}
